package uk.co.thirstybear.simpleapp.db;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import uk.co.thirstybear.simpleapp.model.User;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
class UserRepoTest {
  @Autowired // has to be autowired
  UserRepo userRepo;

  @Test
  public void savesUserToDatabase() {
    User aUser = new User("testname");

    userRepo.save(aUser);
    userRepo.flush();

    assertThat(userRepo.findAll(), contains(aUser));
  }

}