package uk.co.thirstybear.simpleapp.services;

import org.junit.jupiter.api.Test;
import uk.co.thirstybear.simpleapp.db.UserRepo;
import uk.co.thirstybear.simpleapp.model.User;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UserServiceTest {
  @Test
  public void readsUserFromRepo() {
    User expectedUser = new User(1L);
    UserRepo mockUserRepo = mock(UserRepo.class);
    when(mockUserRepo.findById(1L)).thenReturn(Optional.of(expectedUser));

    assertThat(new UserService(mockUserRepo).fetchUserById(1L), is(expectedUser));
  }

}