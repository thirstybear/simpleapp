package uk.co.thirstybear.simpleapp.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uk.co.thirstybear.simpleapp.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
}
