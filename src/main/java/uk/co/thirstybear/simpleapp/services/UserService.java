package uk.co.thirstybear.simpleapp.services;

import org.springframework.stereotype.Service;
import uk.co.thirstybear.simpleapp.db.UserRepo;
import uk.co.thirstybear.simpleapp.model.User;

@Service
public class UserService {
  private final UserRepo userRepo;

  public UserService(UserRepo userRepo) {
    this.userRepo = userRepo;
  }

  public User fetchUserById(long id) {
    return userRepo.findById(id).orElse(User.EMPTY);
  }
}
