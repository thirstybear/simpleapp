package uk.co.thirstybear.simpleapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {
  public static final User EMPTY = new User(-1);

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id; // PK
  private String name;

  public User(long id) {
    this.id = id;
  }

  public User(String name) {
    this.name = name;
  }

  protected User() {
    // required by JPA
  }

  public String getName() {
    return name;
  }
}
